import Vue from "vue";
import Vuex from "vuex";
import {
  CREATE_QUESTION,
  INSERT_QUESTIONS,
  CREATE_RESPONSE,
} from "@/store/mutation-types";
import { http } from "@/services";
import { message } from "@/utils/constant";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    questions: [],
  },
  getters: {
    getUserQuestions: (state) => {
      return state.questions.filter((question) => question.responses.length);
    },
  },
  mutations: {
    [CREATE_QUESTION](state, question) {
      state.questions.push(question);
    },
    [INSERT_QUESTIONS](state, questions) {
      state.questions = questions;
    },
    [CREATE_RESPONSE](state, payload) {
      state.questions.map((question) =>
        question.id === payload.question.id
          ? {
              ...question,
              responses: question.responses.push(payload.title),
            }
          : question
      );
    },
  },
  actions: {
    getAllQuestions: async ({ commit }) => {
      try {
        const response = await http({
          url: "questions/",
          method: "GET",
        });
        commit(INSERT_QUESTIONS, response.data);
      } catch (reason) {
        console.log("get questions error: ", reason);
      }
    },
    createQuestion: async ({ commit }, title) => {
      try {
        const response = await http({
          url: "question/create/",
          method: "POST",
          data: {
            title,
          },
        });
        commit(CREATE_QUESTION, response.data);
        Vue.$toast.success(message.sendQuestionSuccess);
      } catch (reason) {
        Vue.$toast.error(message.fetchError);
      }
    },
    createResponse: async ({ commit }, payload) => {
      try {
        const response = await http({
          url: "response/create/",
          method: "POST",
          data: {
            title: payload.response,
            question: {
              id: payload.question.id,
              title: payload.question.title,
            },
          },
        });
        commit(CREATE_RESPONSE, response.data);
        Vue.$toast.success(message.sendResponseSuccess);
      } catch (reason) {
        Vue.$toast.error(message.fetchError);
      }
    },
  },
});
