export const message = {
  fetchError: "Application en maintenance.",
  sendQuestionSuccess:
    "Merci de patienter en attendant la réponse de l'administrateur",
  sendResponseSuccess: "Réponse envoyer",
};
