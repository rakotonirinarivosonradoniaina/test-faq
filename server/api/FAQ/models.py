from django.db import models


class Question(models.Model):
    title = models.CharField(max_length=1000)
    created_at = models.DateField(auto_created=True, auto_now_add=True)
    updated_at = models.DateField(auto_created=True, auto_now_add=True)

    def __str__(self):
        return self.title


class Response(models.Model):
    title = models.CharField(max_length=1000)
    created_at = models.DateField(auto_created=True, auto_now_add=True)
    updated_at = models.DateField(auto_created=True, auto_now_add=True)
    question = models.ForeignKey(
        Question,
        related_name="responses",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.title




