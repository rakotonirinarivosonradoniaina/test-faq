from rest_framework import serializers

from FAQ.models import Question
from FAQ.models import Response


class QuestionSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    title = serializers.CharField(required=True, max_length=1000, allow_blank=False, allow_null=False)
    created_at = serializers.DateField(required=False, read_only=True)
    updated_at = serializers.DateField(required=False, read_only=True)
    responses = serializers.SlugRelatedField(many=True, read_only=True, slug_field='title')

    def create(self, validated_data):
        return Question.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.save()
        return instance


class ResponseSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(required=True, max_length=1000, allow_blank=False, allow_null=False)
    created_at = serializers.DateField(required=False, read_only=True)
    updated_at = serializers.DateField(required=False, read_only=True)
    question = QuestionSerializer(many=False)

    def create(self, validated_data):
        question_data = validated_data.pop('question')
        current_question = Question.objects.get(pk=question_data['id'])
        response = Response.objects.create(question=current_question, **validated_data)
        return response

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.save()
        return instance






