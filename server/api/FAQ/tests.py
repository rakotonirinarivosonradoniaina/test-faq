from django.test import TestCase

from FAQ.models import Question
from FAQ.models import Response

TEST_QUESTION = "This is my test question, how it work ?"
TEST_RESPONSE = "Just do it as simple as possible"


class QuestionTest(TestCase):
    def setUp(self):
        Question.objects.create(title=TEST_QUESTION)

    def test_questions(self):
        question = Question.objects.get(title=TEST_QUESTION)
        self.assertEqual(question.title, TEST_QUESTION)


class ResponseTest(TestCase):
    def setUp(self):
        question = Question.objects.create(title=TEST_QUESTION)
        Response.objects.create(title=TEST_RESPONSE, question=question)

    def test_response(self):
        response = Response.objects.get(title=TEST_RESPONSE)
        self.assertEqual(response.title, TEST_RESPONSE)
        self.assertEqual(response.question.title, TEST_QUESTION)
