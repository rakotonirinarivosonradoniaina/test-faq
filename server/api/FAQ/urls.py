from django.urls import path
from FAQ import views

urlpatterns = [
    path("questions/", views.QuestionsAPIView.as_view(), name="questions"),
    path("question/create/", views.CreateQuestionAPIView.as_view(), name="question_create"),
    path("question/update/", views.UpdateQuestionAPIView.as_view(), name="question_update"),

    path("responses/", views.ResponsesAPIView.as_view(), name="responses"),
    path("response/create/", views.CreateResponseAPIView.as_view(), name="response_create"),

]
