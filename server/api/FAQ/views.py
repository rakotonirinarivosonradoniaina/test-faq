from rest_framework.generics import ListAPIView
from rest_framework.generics import CreateAPIView
from rest_framework.generics import UpdateAPIView

from FAQ.models import Question
from FAQ.models import Response

from FAQ.serializers import QuestionSerializer
from FAQ.serializers import ResponseSerializer


class QuestionsAPIView(ListAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class CreateQuestionAPIView(CreateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class UpdateQuestionAPIView(UpdateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class ResponsesAPIView(ListAPIView):
    queryset = Response.objects.all()
    serializer_class = ResponseSerializer


class CreateResponseAPIView(CreateAPIView):
    queryset = Response.objects.all()
    serializer_class = ResponseSerializer
